<?php

namespace Database\Seeders;

use App\Models\Pixel;
use Illuminate\Database\Seeder;
use App\Models\GlobalPostback;

class AutoPixel extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Global Pixels for CRM Everflow

        GlobalPostback::create([
            'postback' => 'https://www.google.com',
        ]);


        //Global Pixels for affiliates

        Pixel::create([
            'affiliate' => 'ok',
            'step' => '1',
            'type' => 'postback',
            'pixel' => 'https://track.track-it-all.com/postback?clickid={clickid}',

        ]);

        Pixel::create([
            'affiliate' => 'ok',
            'step' => '1',
            'type' => 'postback',
            'pixel' => 'https://www.p0stb4ck.com/?nid=1134&transaction_id={clickid}',

        ]);

        Pixel::create([
            'affiliate' => 'ad',
            'step' => '1',
            'type' => 'postback',
            'pixel' => 'https://pixel.ethtrck.com/offers/5070?transaction_id={clickid}',

        ]);

        Pixel::create([
            'affiliate' => 'ad',
            'step' => '1',
            'type' => 'iframe',
            'pixel' => "<iframe src='https://pixel.ethtrck.com/offers/5071?transaction_id={clickid}' scrolling='no' frameborder='0' width='1' height='1'></iframe>",

        ]);

        Pixel::create([
            'affiliate' => 'nv',
            'step' => '1',
            'type' => 'postback',
            'pixel' => 'https://www.y134trk.com/?nid=959&transaction_id={clickid}',

        ]);

        Pixel::create([
            'affiliate' => 'yp',
            'step' => '1',
            'type' => 'postback',
            'pixel' => 'http://api.pbydom.com/t/pb?leadid={clickid}',

        ]);

        Pixel::create([
            'affiliate' => 'cd',
            'step' => '1',
            'type' => 'postback',
            'pixel' => 'https://gdmsecure.com/p.ashx?e=1002967&a=9242&f=pb&r={clickid}&key=7217c9537e655b0920c7e018b9309a4ec122b5e95f1064dd38ff1',

        ]);

        Pixel::create([
            'affiliate' => 'gz',
            'step' => '1',
            'type' => 'postback',
            'pixel' => 'https://offr.rocks/p.ashx?f=pb&r={clickid}&t={transaction_id}',

        ]);


    }
}
