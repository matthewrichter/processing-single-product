<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;
use Hash;

class AffiliateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'NuvoMedia',
            'affid' => 'nv',
            'email' => 'mdxsolutionsnv@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);

        User::create([
            'name' => 'admin',
            'affid' => 'admin',
            'email' => 'mdxsolutionsltd@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);

        User::create([
            'name' => 'mdx',
            'affid' => 'mdx',
            'email' => 'mdxsolutionsltdmdx@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);

        User::create([
            'name' => 'internal',
            'affid' => 'internal',
            'email' => 'richter.matthew@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);

        User::create([
            'name' => 'The Fellas',
            'affid' => 'ok',
            'email' => 'ok@mdxsolutions.co.uk',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);

        User::create([
            'name' => 'Yep Ads',
            'affid' => 'yp',
            'email' => 'yp@mdxsolutions.co.uk',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);

        User::create([
            'name' => 'Click Dealer',
            'affid' => 'cd',
            'email' => 'cd@mdxsolutions.co.uk',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);

        User::create([
            'name' => 'Advidi',
            'affid' => 'ad',
            'email' => 'ad@mdxsolutions.co.uk',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);

        User::create([
            'name' => 'Gotzha',
            'affid' => 'gz',
            'email' => 'gz@mdxsolutions.co.uk',
            'email_verified_at' => now(),
            'password' => Hash::make('Teamview@2'),
            'remember_token' => Str::random(10),
        ]);
    }
}
