<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AffiliateSeeder::class);
        $this->call(LanderSeeder::class);
        $this->call(RedirectSeeder::class);
        $this->call(AutoPixel::class);
        $this->call(CheckoutImages::class);
    }
}
