<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Redirect;

class RedirectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Redirect::create([
            'link_bin' => 'https://clickyoda.com/yi5gf',
            'link_success' => 'https://clickyoda.com/yi5gf',
        ]);
    }
}
