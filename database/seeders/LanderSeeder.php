<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Lander;

class LanderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lander::create([
            'title' => 'Demo'.Str::random(10),
            'html' => '<h1>Welcome, this is your demo landing page</h1>',
        ]);
    }
}
