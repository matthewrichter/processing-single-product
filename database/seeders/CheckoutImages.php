<?php

namespace Database\Seeders;

use App\Models\CheckoutImage;
use Illuminate\Database\Seeder;

class CheckoutImages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CheckoutImage::create([
            'image_name' => 'Amazon',
            'file_name' => 'amazon.png',
            'url' => 'images/amazon.png',
            'active' => '0',
        ]);
        CheckoutImage::create([
            'image_name' => 'Apple',
            'file_name' => 'apple.png',
            'url' => 'images/apple.png',
            'active' => '0',
        ]);
        CheckoutImage::create([
            'image_name' => 'Food',
            'file_name' => 'food.png',
            'url' => 'images/food.png',
            'active' => '0',
        ]);
        CheckoutImage::create([
            'image_name' => 'Payment',
            'file_name' => 'payment.jpg',
            'url' => 'images/payment,jpg',
            'active' => '1',
        ]);
        CheckoutImage::create([
            'image_name' => 'Samsung',
            'file_name' => 'samsung.png',
            'url' => 'images/samsung.png',
            'active' => '0',
        ]);
        CheckoutImage::create([
            'image_name' => 'Playstation',
            'file_name' => 'playstation.png',
            'url' => 'images/playstation.png',
            'active' => '0',
        ]);
        CheckoutImage::create([
            'image_name' => 'XBOX',
            'file_name' => 'xbox.jpg',
            'url' => 'images/xbox.jpg',
            'active' => '0',
        ]);
    }
}
