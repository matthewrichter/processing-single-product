<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('affid');
            $table->string('user_id');
            $table->string('clickid');
            $table->string('device')->default('desktop');
            $table->integer('campaign_id');
            $table->integer('campaign_upsell_id');
            $table->integer('product_id');
            $table->integer('shipping_id');
            $table->integer('upsell_id');
            $table->integer('upsell_shipping_id');
            $table->integer('ipaddress');
            $table->integer('c1');
            $table->integer('c2');
            $table->integer('c3');
            $table->integer('conversion')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
