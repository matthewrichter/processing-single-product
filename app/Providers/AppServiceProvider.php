<?php

namespace App\Providers;

use App\Models\CheckoutImage;
use Illuminate\Support\ServiceProvider;
use App\Observers\UserObserver;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        //Default checkout image
//        $image = CheckoutImage::where('active', 1)->first();
//        config(['app.selected_image' => $image['file_name']]);
    }
}
