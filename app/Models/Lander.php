<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lander extends Model
{
    protected $fillable = ['html'];
}
