<?php

namespace App\Limelight;
use Illuminate\Http\Request;
use Log;

class BuildPayload{

    public function buildPayload(Request $request){

        $data = '
        {
              "firstName":"'.$request->firstName.'",
              "lastName":"'.$request->lastName.'",
              "billingFirstName":"'.$request->firstName.'",
              "billingLastName":"'.$request->lastName.'",
              "billingAddress1":"'.$request->address1.'",
              "billingAddress2":"'.$request->address1.'",
              "billingCity":"'.$request->city.'",
              "billingState":"London",
              "billingZip":"'.$request->postcode.'",
              "shippingFirstName":"'.$request->firstName.'",
              "shippingLastName":"'.$request->lastName.'",
              "shippingAddress1":"'.$request->address1.'",
              "shippingAddress2":"'.$request->address1.'",
              "shippingCity":"'.$request->city.'",
              "shippingState":"London",
              "shippingZip":"'.$request->postcode.'",
              "billingCountry":"GB",
              "shippingCountry":"GB",
              "phone":"'.$request->phone.'",
              "email":"'.$request->email.'",
              "creditCardType":"VISA",
              "creditCardNumber":"'.$request->cardNumber.'",
              "expirationDate":"'.$request->expiryMonth.$request->expiryYear.'",
              "CVV":"'.$request->cvv.'",
              "shippingId":"'.session('campaign_step1_ship').'",
              "tranType":"Sale",
              "ipAddress":"'.session('ip').'",
              "campaignId":"'.session('campaign_step1').'",
              "productId":"'.session('campaign_product').'",
              "AFID":"'.session('affid').'",
              "SID":"'.session('c1').'",
              "AFFID":"'.session('affid').'",
              "C1":"'.session('c1').'",
              "C2":"'.session('c2').'",
              "C3":"'.session('c3').'",
              "AID":"AID",
              "OPT":"OPT",
              "click_id":"'.session('clickid').'",
              "billingSameAsShipping":"YES",
              "sessionId":"'.session('clickid').'",
              "device_category":"NA",
              "cavv":"'.$request->cavv.'",
              "eci":"'.$request->eci.'",
              "ds_trans_id":"'.$request->ds_trans_id.'",
              "acs_trans_id":"'.$request->acs_trans_id.'",
              "3d_version":"2.1.0",
              "sessionId":"'.session('clickid').'"

        }
               ';

        log::debug($data);

        //Add Contact to SMS List

        $phone = strval(intval($request->phone)); //Trim the 0
        $send = new \App\SMS\AddContactsToList();
        $send->addContact("44".$phone, $request->firstName, $request->lastName);

        return $data;

    }


}