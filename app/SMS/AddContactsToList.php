<?php

namespace App\SMS;

use Carbon\Carbon;

class AddContactsToList
{

    public function addContact($number, $firstName, $lastName)
    {

        $url = 'https://api.voodoosms.com/contacts';
        $date = Carbon::now()->format('d-m-Y');

        $data = '{
                    "name": "'.$date.'",
                    "contacts": [
                {
                    "number": "'.$number.'",
                    "dynamic_data": [
                {
                    "First Name": "'.$firstName.'",
                    "Surname": "'.$lastName.'",
                    "Page": "'.env("APP_URL").'"
                }]}]}';

        $request_headers = array();
        $request_headers[] = 'Content-Type: application/json';
        $request_headers[] = 'Authorization: Bearer sfW4BAxvbIxt12redajoY5eDXtP2ntiUO6PQ6CCYK0HBtz';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $strResponse = curl_exec($ch);
        curl_close($ch);

        return $strResponse;

    }

}