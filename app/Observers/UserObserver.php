<?php

namespace App\Observers;

use App\User;
use App\Models\Pixel;
use App\Models\Campaign;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $campaign = new Campaign;
        $campaign->affid = $user->affid;
        $campaign->step1 = 1; //default value
        $campaign->upsell = 1; //default value
        $campaign->straight = 1; //default value
        $campaign->step_ship = 1; //default value
        $campaign->upsell_ship = 1; //default value
        $campaign->straight_ship = 1; //default value
        $campaign->save();
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        Campaign::findOrFail($user->id)->delete();
        Pixel::where('affiliate', $user->affid)->delete();
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
