<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Log;
use App\Limelight\BuildUrl;
use App\Limelight\BuildPayload;
use App\Limelight\postData;

class ProcessController extends Controller
{

    public function process(Request $request)
    {

        try {

            $url = new BuildUrl();
            $url = $url->buildUrl('new_order');

            $data = new BuildPayload();
            $data = $data->buildPayload($request);

            $order = new postData();
            $order = $order->postData($url, $data);

            Log::channel('transactions')->info($order);
            Log::channel('transactions')->info($data);


            if ($order['response_code'] == 100) {

                session(['step1-success' => 1]);
                session(['step1-id' => 'transactionID']);
                session(['username' => $request['email']]);
                session(['password' => Hash::make($request['phone'])]);

            }

            return $order;


        } catch (Exception $e) {
            report($e);

            return false;
        }

    }
}
