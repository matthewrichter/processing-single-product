<?php

namespace App\Http\Controllers;

use App\Models\CheckoutImage;
use Illuminate\Http\Request;


class CheckoutImageController extends Controller
{

    public function index(){
        $images = CheckoutImage::all();
        return view('settings.plugins.checkout_image', compact('images'));
    }

    public function edit(Request $request){
        CheckoutImage::query()->update(['active' => 0]);
        CheckoutImage::query()->where('id', $request->checkout_image)->update(['active' => 1]);
        return redirect()->back()->with('success', 'Update Successful');
}

}
