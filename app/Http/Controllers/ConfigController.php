<?php

namespace App\Http\Controllers;

use App\Models\CheckoutImage;
use App\Models\GlobalPostback;
use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\Redirect;
use App\User;

class ConfigController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $users = User::all();
        $redirect = Redirect::all();
        $images = CheckoutImage::all();
        $image_selected = CheckoutImage::where('active', 1)->first();
        $global = GlobalPostback::all();

        return view('settings.config', compact( 'users','redirect','images', 'image_selected', 'global'));

    }

    public function show($id)
    {

        $campaign = Campaign::find($id);

        return view('settings.show', compact('campaign'));

    }

    public function edit(Request $request)
    {

        $productSettings = Campaign::find($request->id);
        $productSettings->affid = $request->affid;
        $productSettings->step1 = $request->step1;
        $productSettings->upsell = $request->upsell;
        $productSettings->straight = $request->straight;
        $productSettings->step_ship = $request->step_ship;
        $productSettings->upsell_ship = $request->upsell_ship;
        $productSettings->straight_ship = $request->straight_ship;
        $productSettings->product_id = $request->product_id;
        $productSettings->upsell_id = $request->upsell_id;
        $productSettings->straight_id = $request->id;
        $productSettings->save();

        return redirect()->back()->with('success', 'Update Successful');
    }

    public function destroy($id)
    {

        User::findOrFail($id)->delete();

        return redirect()->back()->with('success', 'Deleted Successfully');

    }

    public function redirect(Request $request)
    {

        $redirect = Redirect::first();
        $redirect->link_bin = $request->link_bin;
        $redirect->link_success = $request->link_success;
        $redirect->save();

        return redirect()->back()->with('success', 'Update Successful');
    }


}
