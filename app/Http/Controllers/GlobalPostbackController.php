<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GlobalPostback;

class GlobalPostbackController extends Controller
{

    public function update(Request $request){
        GlobalPostback::query()->where('id', 1)->update(['postback' => $request->global]);
        return redirect()->back()->with('success', 'Postback Update Successful');
    }

}
