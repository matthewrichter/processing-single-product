<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Limelight\BuildUrl;
use App\Limelight\BuildPayload;
use App\Limelight\postData;

class TestingController extends Controller
{

    public function index(){
        return view('testing.3ds_v2');
    }

    public function lander(){
        return view('desktop.test.index');
    }

    public function addProspect(Request $request){

        $prospect = new BuildUrl();
        $url = $prospect->buildUrl('new_prospect');
        $data = json_encode($request->all());

        $post = new postData();
        $prospectID = $post->postData($url, $data);

        $prospect = new BuildUrl();
        $url = $prospect->buildUrl('prospect_view');
        $prospect = '{"prospect_id":"'.$prospectID['prospectId'].'"}';
        $post = new postData();
        $prospect = $post->postData($url, $prospect);

        return view('testing.3ds_v2', compact('prospect'));

    }

    public function process(Request $request){

        $url = new BuildUrl();
        $url = $url->buildUrl('new_order');

        $data = new BuildPayload();
        $data = $data->buildPayload($request);

        $post = new postData();
        $post = $post->postData($url, $data);

        return $post;


    }

}
