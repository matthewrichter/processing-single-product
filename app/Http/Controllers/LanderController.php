<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lander;

class LanderController extends Controller
{

    public function index($id){

        $lander = Lander::find($id);

        return view('desktop.index', compact('lander'));
    }

    public function show(){

        $landers = Lander::get();

        return view('settings.landers', compact('landers'));
    }

    public function edit($id){

        $lander = Lander::find($id);

        return view('settings.edit_lander', compact('lander'));
    }

    public function save(Request $request){

        $lander = Lander::find($request->id);
        $lander->html = $request->html;
        $lander->save();

        return redirect()->back()->with('success', 'Update Successful');

    }

}
