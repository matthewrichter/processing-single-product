<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pixel;

class PixelController extends Controller
{

    public function show($affid){

        $pixels = Pixel::where('affiliate', $affid)->get();

        return view('settings.pixels', compact( 'pixels', 'affid'));

    }

    public function create(Request $request){

        $pixel = new Pixel;
        $pixel->affiliate = $request->affiliate;
        $pixel->step = $request->step;
        $pixel->type = $request->type;
        $pixel->pixel = $request->pixel;
        $pixel->save();

        return redirect()->back()->with('success', 'Pixel Added Successfully');

    }

}
