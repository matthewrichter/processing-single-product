<?php

namespace App\Http\Middleware\Custom;

use Closure;
use App\Counter\Count;
use App\Models\Counter;
use App\Counter\SetMDX;
use App\Counter\SetSession;
use App\Campaign\SetCampaign;


class setSessions
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $countext
     * @return mixed
     */
    public function handle($request, Closure $countext){

        $count = new Count();
        $count = $count->counter();

        if ($count == true) {

            SetMDX::set();
            Counter::truncate();

        }else{

            SetSession::set();

        }

        SetCampaign::set();

        return $countext($request);
    }
}
