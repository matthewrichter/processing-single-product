<?php

namespace App\Http\Middleware\Custom;

use Closure;
use App\User;
use Illuminate\Support\Facades\Log;

class verifyAffiliate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $affiliate = User::where('affid', $request->affid)->where('is_active', true)->first();

        if(!$affiliate){

            Log::error('Affiliate '.$request->affid. ' does not exist or is not active.');

            return response()->json([
                'message' => 'Affiliate does not exist or is not active',
                'code' => 401
            ], 401);

        }

        return $next($request);
    }
}
