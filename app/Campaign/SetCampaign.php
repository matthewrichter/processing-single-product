<?php

namespace App\Campaign;

use Session;
use App\Models\Campaign;

class SetCampaign {

    public static function set(){

        $camapaign = Campaign::where('affid', session('affid'))->first();

        if(session('affid') == $camapaign->affid ?? 'mdx'){$campaign_step1 = $camapaign->step1  ?? 1;}
        if(session('affid') == $camapaign->affid ?? 'mdx'){$campaign_upsell = $camapaign->upsell  ?? 1;}
        if(session('affid') == $camapaign->affid ?? 'mdx'){$campaign_straight = $camapaign->straight  ?? 1;}
        if(session('affid') == $camapaign->affid ?? 'mdx'){$campaign_step1_ship = $camapaign->step_ship  ?? 1;}
        if(session('affid') == $camapaign->affid ?? 'mdx'){$campaign_upsell_ship = $camapaign->upsell_ship  ?? 1;}
        if(session('affid') == $camapaign->affid ?? 'mdx'){$campaign_straight_ship = $camapaign->straight_ship  ?? 1;}
        if(session('affid') == $camapaign->affid ?? 'mdx'){$campaign_product = $camapaign->product_id  ?? 1;}

        Session::put(['campaign_step1' => $campaign_step1]);
        Session::put(['campaign_upsell' => $campaign_upsell]);
        Session::put(['campaign_straight' => $campaign_straight]);
        Session::put(['campaign_step1_ship' => $campaign_step1_ship ]);
        Session::put(['campaign_upsell_ship' => $campaign_upsell_ship ]);
        Session::put(['campaign_straight_ship' => $campaign_straight_ship ]);
        Session::put(['campaign_product' => $campaign_product ]);

    }
}