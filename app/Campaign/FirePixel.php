<?php

namespace App\Campaign;

use Session;
use App\Models\Pixel;
use App\Models\GlobalPostback;
use Log;

class FirePixel {

    private static $state = null;
    private static $pixel = null;
    private static $transactionID = null;
    private static $clickID  = null;
    private static $editPixel  = null;
    private static $pixelData  = null;
    private static $c1  = null;
    private static $c2  = null;
    private static $c3  = null;

    static function pixelStep1() {

        self::$state = session('step1-success');

        if(self::$state == 1) {

            try {

                self::$pixel = Pixel::where('affiliate', session('affid'))->where('step', '=', 1)->where('type', '=', 'iframe')->get();

                foreach (self::$pixel as $key) {
                    self::$transactionID = session('step1-id');
                    self::$clickID = session('clickid');
                    self::$editPixel = str_replace("{transaction_id}", self::$transactionID, $key->pixel);
                    self::$pixelData = str_replace("{clickid}", self::$clickID, self::$editPixel);

                    echo self::$pixelData;
                    Log::channel('pixels')->info('Iframe Step1 Order ID: '.self::$transactionID. ' - '.self::$pixelData);

                }

                self::$pixel = Pixel::where('affiliate', session('affid'))->where('step', '=', 1)->where('type', '=', 'postback')->get();

                foreach (self::$pixel as $key) {
                    self::$transactionID = session('step1-id');
                    self::$c1 = session('c1');
                    self::$c2 = session('c2');
                    self::$c3 = session('c3');
                    self::$clickID = session('clickid');
                    self::$editPixel = str_replace("{transaction_id}", self::$transactionID, $key->pixel);
                    self::$editPixel = str_replace("{c1}", self::$c1, self::$editPixel);
                    self::$editPixel = str_replace("{c2}", self::$c2, self::$editPixel);
                    self::$editPixel = str_replace("{c3}", self::$c3, self::$editPixel);
                    self::$pixelData = str_replace("{clickid}", self::$clickID, self::$editPixel);

                    $post = [];
                    $ch = curl_init(self::$pixelData);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    Log::channel('pixels')->info('Postback: '.self::$pixelData);
                    Log::channel('pixels')->info('Response Postback Step1 Order ID: '.self::$transactionID. ' - '.$response);

                }

            } catch (Exception $e) {
                report($e);


                return false;
            }
        }

        if(self::$state == 1) {

            $global = GlobalPostback::find(1);
            $global = $global->postback;

            $post = [];
            $ch = curl_init($global);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $response = curl_exec($ch);
            curl_close($ch);

            Log::channel('pixels')->info('Global Postback: '.$global);

        }
    }

    static function pixelStep2() {

        self::$state = session('step2-success');

        if(self::$state == 1) {

            try {

                self::$pixel = Pixel::where('affiliate', session('affid'))->where('step', '=', 2)->where('type', '=', 'iframe')->get();

                foreach (self::$pixel as $key) {
                    self::$transactionID = session('step2-id');
                    self::$clickID = session('clickid');
                    self::$editPixel = str_replace("{transaction_id}", self::$transactionID, $key->pixel);
                    self::$pixelData = str_replace("{clickid}", self::$clickID, self::$editPixel);

                    echo self::$pixelData;
                    Log::channel('pixels')->info('Iframe Step2 Order ID: '.self::$transactionID. ' - '.self::$pixelData);

                }

                self::$pixel = Pixel::where('affiliate', session('affid'))->where('step', '=', 2)->where('type', '=', 'postback')->get();

                foreach (self::$pixel as $key) {
                    self::$transactionID = session('step2-id');
                    self::$clickID = session('clickid');
                    self::$editPixel = str_replace("{transaction_id}", self::$transactionID, $key->pixel);
                    self::$pixelData = str_replace("{clickid}", self::$clickID, self::$editPixel);

                    $post = [];
                    $ch = curl_init(self::$pixelData);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    Log::channel('pixels')->info('Postback: '.self::$pixelData);
                    Log::channel('pixels')->info('Response Postback Step2 Order ID: '.self::$transactionID. ' - '.$response);


                }

            } catch (Exception $e) {
                report($e);


                return false;
            }
        }
    }
}