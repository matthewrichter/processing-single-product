<?php

namespace App\Counter;

use Session;
use Request;

class SetSession {

    public static function set(){

        $ip = $_SERVER['REMOTE_ADDR'];

        Session::put(['affid' => !empty(Request::get('affid')) ? Request::get('affid') : 'mdx' ]);
        Session::put(['c1' => !empty(Request::get('c1')) ? Request::get('c1') : 'mdx_c1' ]);
        Session::put(['c2' => !empty(Request::get('c2')) ? Request::get('c2') : 'mdx_c2' ]);
        Session::put(['c3' => !empty(Request::get('c3')) ? Request::get('c3') : 'mdx_c3' ]);
        Session::put(['clickid' => !empty(Request::get('clickid')) ? Request::get('clickid') : 'mdx_clickid' ]);
        Session::put(['ip' => $ip]);

    }
}