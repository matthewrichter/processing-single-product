<?php

namespace App\Counter;

use App\Models\Counter;

class Count {

    public function counter(){

        $count = new Counter;
        $count->clickid = 1;
        $count->save();

        $x = Counter::count();

        while ($x > 0)
            $x = $x - env('S_AMOUNT');

        if ($x == 0)
        return true;

        return false;

    }
}