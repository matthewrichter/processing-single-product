<?php

namespace App\Counter;

use Session;

class SetMDX {

    public static function set(){

        $ip = $_SERVER['REMOTE_ADDR'];

        Session::put(['affid' => 'mdx']);
        Session::put(['c1' => 'Pixel']);
        Session::put(['c2' => 'Pixel']);
        Session::put(['c3' => 'Pixel' ]);
        Session::put(['clickid' => 'Pixel']);
        Session::put(['ip' => $ip]);

    }
}