<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

//Testing
Route::prefix('testing')->group(function () {
    Route::get('3ds', 'TestingController@index')->name('test_3ds');
//    Route::post('process', 'TestingController@process')->name('process_3ds');
    Route::post('checkout', 'TestingController@addProspect')->name('test_add_prospect');
    Route::get('lander', 'TestingController@lander')->middleware('count.leads')->name('test_lander');
});
Auth::routes();

Route::prefix('settings')->group(function () {
    Route::get('config', 'ConfigController@index')->name('config');
    Route::post('update_redirect', 'ConfigController@redirect')->name('update_redirect');
    Route::get('config/{id}', 'ConfigController@show')->name('config_show');
    Route::put('config', 'ConfigController@edit')->name('config_update');
    Route::delete('config/{id}', 'ConfigController@destroy')->name('config_delete');
    Route::get('pixels/{affid}', 'PixelController@show')->name('pixels_show');
    Route::patch('/image/update', 'CheckoutImageController@edit')->name('update_image');
    Route::patch('/global/postback/update', 'GlobalPostbackController@update')->name('update_global_postback');
});

Route::prefix('settings')->group(function () {
    Route::get('pixels/{affid}', 'PixelController@show')->name('pixels_show');
});

Route::prefix('settings')->group(function () {
    Route::post('add_user', 'UserController@create')->name('add_user');
    Route::post('add_pixel', 'PixelController@create')->name('add_pixel');
    Route::get('/lander/show', 'LanderController@show')->name('lander_show');
    Route::get('/lander/edit/{id}', 'LanderController@edit')->name('lander_edit');
    Route::patch('/lander/save', 'LanderController@save')->name('html_save');
});

//Live Pages
Route::prefix('lander')->group(function () {
    Route::get('/{id}', 'LanderController@index')->middleware('count.leads')->name('lander');
    Route::post('/checkout', 'CheckoutController@index')->name('checkout');
});

Route::get('/checkout', 'CheckoutController@index')->name('checkout');
Route::post('/process', 'ProcessController@process')->name('live_process_3ds');
Route::get('/thankyou', function(){ $redirects = \App\Models\Redirect::all(); return view('desktop.thankyou')->with(compact('redirects'));});
Route::get('/redirect', function(){ $redirects = \App\Models\Redirect::all(); return view('desktop.redirect')->with(compact('redirects'));})->name('redirect');

Route::get('3ds', function(){
    return view('desktop.3ds');
});