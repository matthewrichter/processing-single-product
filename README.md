# Installation Instructions New Laravel 8 Upgrade
Follow the instructions below in order to install the processing site.

> npm will need to be installed and f-open enabled in php.ini

### Repository
> git clone https://matthewrichter@bitbucket.org/matthewrichter/main_process_v8.git

### Auto Install
> run ./auto_install 

> you will need your database details for this to work

### MANUAL INSTALL
### Dependencies
> composer update

> npm install

### Permissions
Only change permissions if cloned from root

> chown -R {domain_user} main_process

> chmod -R 755 main_process

### Setup Laravel

> Create .env file

> php artisan migrate

> php artisan db:seed

>Remove the larval log file located in storage folder

> Edit .htaccess redirect

##### Edit .htaccess redirect

* RewriteEngine on
* RewriteCond %{REQUEST_URI} !^public
* RewriteRule ^(.*)$ main_process_v8/public/$1 [L]

##### Uncomment 

* App/Providers/AppServiceProviders
 >         $image = CheckoutImage::where('active', 1)->first();
 >         config(['app.selected_image' => $image['file_name']]);

* ADMIN:
https://domain/settings/config

* Landing Page:
https://domain/lander/1?affid={affiliate_id}