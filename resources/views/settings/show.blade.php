@extends('layouts.settings')

@section('content')

    <div class="container">

        @include('settings.plugins.configuration_settings')

    </div>

@endsection