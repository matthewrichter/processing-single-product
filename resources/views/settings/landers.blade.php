@extends('layouts.settings')

@section('content')
    <br>
    <div class="container">
        @include('settings.plugins.alerts')
        @include('settings.plugins.landers')
    </div>

@endsection
