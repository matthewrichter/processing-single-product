@extends('layouts.settings')

@section('content')
    <br>
    <div class="container">
        @include('settings.plugins.alerts')
        <form action="{{ route('html_save') }}" method="POST">
            @method('PATCH')
            @csrf
        <input type="hidden" name="id" value="{{ $lander->id }}">
        <div class="card">
            <div class="card-header">Edit {{ $lander->title }}</div>
            <div class="card-body">
                <div class="form-row">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <textarea class="form-control" name="html" id="html" rows="20">{!!html_entity_decode($lander->html)!!}</textarea>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">UPDATE HTML</button>
                <a href="{{ route('lander_show') }}" class="btn btn-primary">BACK</a>
            </div>
        </div>
        </form>
    </div>

@endsection

