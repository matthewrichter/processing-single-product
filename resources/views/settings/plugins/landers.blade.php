<br><br>
<div class="card">
    <div class="card-header">Current Landers</div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>

            @if($landers)
                @foreach($landers as $lander)
                    <tr>
                        <td>{{ $lander->id }}</td>
                        <td>{{ $lander->title }}</td>
                        <td><a href="{{ route('lander_edit', $lander->id) }}" class="btn btn-primary">EDIT</a></td>
                        <td>
                            <form action="{{ route('config_delete', $lander->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm confirm">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif

            @if(!$landers)
                <tr><td>No landers have been setup</td><td></td><td></td><td></td><td></td><td></td></tr>
            @endif

            </tbody>
        </table>
        <a href="{{ route('config') }}" class="btn btn-primary">BACK</a>
    </div>
</div>