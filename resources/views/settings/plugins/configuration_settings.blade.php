
<p></p>
@include('settings.plugins.alerts')

<form action="{{ route('config_update') }}" method="POST">

    @method('PUT')
    @csrf

    <input type="hidden" name="affid" value="{{ $campaign->affid }}">
    <input type="hidden" name="id" value="{{ $campaign->id }}">

    <div class="card">
        <div class="card-header">Product Settings</div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Step 1 Campaign ID</label>
                    <input type="text" class="form-control" id="step1" name="step1" value="{{ $campaign->step1 }}">
                </div>
                <div class="form-group col-md-4">
                    <label>Upsell Campaign ID</label>
                    <input type="text" class="form-control" id="upsell" name="upsell" value="{{ $campaign->upsell }}">
                </div>
                <div class="form-group col-md-4">
                    <label>Straight Campaign ID</label>
                    <input type="text" class="form-control" id="straight" name="straight" value="{{ $campaign->straight }}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Step 1 Shipping ID</label>
                    <input type="text" class="form-control" id="step1_ship" name="step_ship" value="{{ $campaign->step_ship }}">
                </div>
                <div class="form-group col-md-4">
                    <label>Upsell Shipping ID</label>
                    <input type="text" class="form-control" id="upsell_ship" name="upsell_ship" value="{{ $campaign->upsell_ship }}">
                </div> <div class="form-group col-md-4">
                    <label>Straight Shipping ID</label>
                    <input type="text" class="form-control" id="straight_ship" name="straight_ship" value="{{ $campaign->straight_ship }}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Step 1 Product ID</label>
                    <input type="text" class="form-control" id="product_id" name="product_id" value="{{ $campaign->product_id }}">
                </div>
                <div class="form-group col-md-4">
                    <label>Upsell Product ID</label>
                    <input type="text" class="form-control" id="upsell_id" name="upsell_id" value="{{ $campaign->upsell_id }}">
                </div> <div class="form-group col-md-4">
                    <label>Straight Product ID</label>
                    <input type="text" class="form-control" id="straight_id" name="straight_id" value="{{ $campaign->straight_id }}">
                </div>
            </div>

            <button type="submit" class="btn btn-primary">UPDATE</button>
            <a href="{{ route('config') }}" class="btn btn-primary">BACK</a>
        </div>
    </div>
</form>
<p></p>