<p></p>

@include('settings.plugins.errors')

<form method="POST" action="{{ route('update_global_postback') }}">
    @method('PATCH')
    @csrf

    <div class="card">
        <div class="card-header">Edit Global Postback</div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <input type="text" value="{{ $global[0]->postback ?? '' }}" class="form-control" id="global" name="global">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">UPDATE</button>
        </div>
    </div>
</form>
<p></p>