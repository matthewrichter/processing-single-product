<br><br>
<div class="card">
    <div class="card-header">Current Affiliates</div>
    <div class="card-body">

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Affiliate</th>
                <th scope="col">Email</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $user)

                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->affid }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->is_active }}</td>
                    <td><a href="{{ route('config_update') }}/{{ $user->id }}" class="btn btn-primary btn-sm">Settings</a>
                        <a href="pixels/{{ $user->affid }}" class="btn btn-warning btn-sm">Pixels</a>
                    </td>
                    <td>
                        <form action="{{ route('config_delete', $user->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-sm confirm">Delete</button>
                        </form>
                    </td>
                </tr>

            @endforeach

            </tbody>
        </table>
    </div>
</div>