<p></p>

@include('settings.plugins.errors')

<form method="POST" action="{{ route('update_image') }}">

    @method('PATCH')
    @csrf

    <div class="card">
        <div class="card-header">Select Checkout Image</div>
        <div class="card-body">
                <p>Currently Active: {{ $image_selected['image_name'] }}</p>
                <div class="form-group">
                    <select class="form-control" id="checkout_image" name="checkout_image">
                        @foreach($images as $image)
                            <option value="{{ $image->id }}">{{ $image->image_name }}</option>
                        @endforeach
                    </select>
            </div>
            <button type="submit" class="btn btn-primary">UPDATE</button>
        </div>
    </div>
</form>
<p></p>