<p></p>

@include('settings.plugins.errors')

<form method="POST" action="{{ route('update_redirect') }}">

    @csrf

    <div class="card">
        <div class="card-header">Edit Redirects</div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Bin Block</label>
                    <input type="text" value="{{ $redirect[0]->link_bin ?? '' }}" class="form-control" id="link_bin" name="link_bin">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Success</label>
                    <input type="text" value="{{ $redirect[0]->link_success ?? '' }}" class="form-control" id="link_success" name="link_success">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">UPDATE</button>
        </div>
    </div>
</form>
<p></p>