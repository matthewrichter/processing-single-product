<br><br>
<div class="card">
    <div class="card-header">Current Pixels</div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Affiliate</th>
                <th scope="col">Step</th>
                <th scope="col">Type</th>
                <th scope="col">Pixel</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>

            @if($pixels)
                @foreach($pixels as $pixel)
                    <tr>
                        <td>{{ $pixel->id }}</td>
                        <td>{{ $pixel->affiliate }}</td>
                        <td>{{ $pixel->step }}</td>
                        <td>{{ $pixel->type }}</td>
                        <td>{{ $pixel->pixel }}</td>
                        <td>
                            <form action="{{ route('config_delete', $pixel->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm confirm">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif

            @if(!$pixels)
                <tr><td>No pixels have been setup</td><td></td><td></td><td></td><td></td><td></td></tr>
            @endif

            </tbody>
        </table>
    </div>
</div>

<form method="POST" action="{{ route('add_pixel') }}">

    @csrf

    <input type="hidden" name="affiliate" value="{{ $affid }}">

    <div class="card">
        <div class="card-header">Add New Pixel</div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Step</label>
                        <select class="form-control" name="step" id="exampleFormControlSelect1">
                            <option value="1">Step 1</option>
                            <option value="2">Step 2</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Type</label>
                        <select class="form-control" name="type" id="exampleFormControlSelect1">
                            <option value="iframe">iframe</option>
                            <option value="postback">postback</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="exampleFormControlTextarea1">Pixel</label>
                    <textarea class="form-control" name="pixel" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">ADD NEW PIXEL</button>
            <a href="{{ route('config') }}" class="btn btn-primary">BACK</a>
        </div>
    </div>
</form>
