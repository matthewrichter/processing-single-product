<p></p>

@include('settings.plugins.errors')

<form method="POST" action="{{ route('add_user') }}">

    @csrf

    <div class="card">
        <div class="card-header">Add New Affiliate</div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Affiliate Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group col-md-6">
                    <label>Affiliate ID</label>
                    <input type="text" class="form-control" id="affid" name="affid">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Email</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group col-md-3">
                    <label>Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group col-md-3">
                    <label>Confirm</label>
                    <input type="password" class="form-control" id="password-confirm" name="password-confirmation">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">ADD NEW AFFILIATE</button>
        </div>
    </div>
</form>
<p></p>