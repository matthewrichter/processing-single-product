<p></p>

@include('settings.plugins.errors')

<form method="POST" action="{{ route('add_pixel') }}">

    @csrf

    <input type="hidden" name="affiliate" value="{{ $pixel->affiliate }}">

    <div class="card">
        <div class="card-header">Add New Pixel</div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Step</label>
                        <select class="form-control" name="step" id="exampleFormControlSelect1">
                            <option value="1">Step 1</option>
                            <option value="2">Step 2</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Type</label>
                        <select class="form-control" name="type" id="exampleFormControlSelect1">
                            <option value="iframe">iframe</option>
                            <option value="postback">postback</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="exampleFormControlTextarea1">Pixel</label>
                    <textarea class="form-control" name="pixel" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">ADD NEW PIXEL</button>
        </div>
    </div>
</form>
<p></p>