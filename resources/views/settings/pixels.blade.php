@extends('layouts.settings')

@section('content')

    <div class="container">
        <br>
        @include('settings.plugins.alerts')
        @include('settings.plugins.pixels')

    </div>

@endsection
