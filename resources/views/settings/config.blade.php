@extends('layouts.settings')

@section('content')
    <br>
    <div class="container">
        <div align="right"><a href="{{ route('lander_show') }}" class="btn btn-success">LANDING PAGES</a></div>
        @include('settings.plugins.alerts')
        @include('settings.plugins.current_affiliates')
        @include('settings.plugins.add_affiliate')
        @include('settings.plugins.redirects')
        @include('settings.plugins.checkout_image')
        @include('settings.plugins.global_postback')
    </div>

@endsection
