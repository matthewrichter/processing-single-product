<!DOCTYPE html>
<html>
<head>
    <style>
        .center {
            text-align: center;
        }
    </style>
</head>
<body>

<div class="center">
    <img src="https://map.stjohns.ca/mapcentre/assests/images/loading.gif"  />
    <h3>We currently do not accept this card on this offer, but we have the same offer on a different page.</h3>
    <h2>You are busy being redirected!</h2>
</div>

<meta http-equiv="refresh" content="3; URL='{{ $redirects[0]->link_bin }}'" />

</body>
</html>