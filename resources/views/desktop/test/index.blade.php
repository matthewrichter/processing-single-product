@extends('desktop.test.layouts.main')

@section('content')

    <br><br>
    <div class="container border">

        <br>
        <div class="cart"><h1 align="center">Tell Us Where To Send Your 30 Day Supply</h1></div>
        <br>
        <form action="checkout" method="post">

            @csrf
            <input type="hidden" name="campaignId" value="{{session('campaign_step1')}}">
            <input type="hidden" name="ipAddress" value="{{session('ip')}}">
            <input type="hidden" name="AFFID" value="{{session('affid')}}">
            <input type="hidden" name="C1" value="{{session('c1')}}">
            <input type="hidden" name="click_id" value="{{session('clickid')}}">
            <input type="hidden" name="country" value="GB">

            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">First Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="firstName" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Last Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="lastName" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Postcode:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="zip" minlength="4" maxlength="7" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Address:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="address1" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">City:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="city" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">County:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="state" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Telephone:</label>
                <div class="col-sm-10">
                    <input type="phone" class="form-control" name="phone" minlength="10" maxlength="11" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Email:</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" name="email" required>
                </div>
            </div>
            <div align="center"><button type="submit" class="btn btn-primary btn-lg">RUSH ORDER</button></div>
            <br>

        </form>
    </div>

@endsection