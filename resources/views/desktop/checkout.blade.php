@extends('layouts.checkout')

@section('content')

    <style>

        .container-fluid {
            padding-top: 40px;
        }

        iframe {
            width: 500px;
            height: 500px;
        }

        #showiframe {
            position: absolute;
            top: 90;
            left: 0;
            bottom: 0;
            right: 0;
            width: 100%;
            height: 60%;
            border: 0;
        }

        .bg-image {
            background-image: url("../images/{{ config('app.selected_image') }}");
            background-size: cover;
            background-position: center;
        }

    </style>

    <body>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="d-none d-md-flex col-md-4 col-lg-4 bg-image"></div>
            <div class="col-md-6 col-lg-8">
                <div class="login d-flex align-items-right py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-lg-6 mx-auto">
                                <div align="center">
                                    <img src="images/secure.png" width="120px">
                                    <img src="images/secure2.png" width="120px">
                                </div>
                                <form id="billing-form" role="form" onsubmit="process()" >

                                    <input type="hidden" name="x_transaction_id" value="{{ rand(100000, 999999) }}" data-threeds="id" />
                                    <input type="hidden" name="x_amount" value="{{ env('SHIPPING') }}" data-threeds="amount" />

                                    <hr>
                                    <h4>Personal Details</h4>
                                    <hr>

                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </div>
                                        <input name="first_name" id="first_name" class="form-control" placeholder="First Name" value="{{!empty($_GET['firstname']) ? $_GET['firstname'] : '' }}" type="text" v-model="first_name" required />
                                    </div>

                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </div>
                                        <input name="last_name" id="last_name" class="form-control" placeholder="Last Name" value="{{!empty($_GET['lastname']) ? $_GET['lastname'] : '' }}" type="text" v-model="last_name" required />
                                    </div>


                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-envelope"></i></span>
                                        </div>
                                        <input name="email" id="email" class="form-control" placeholder="Email Address" type="email" value="{{!empty($_GET['email']) ? $_GET['email'] : '' }}" v-model="email" required />
                                    </div>


                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-phone"></i>
                                            </span>
                                        </div>

                                        <select class="custom-select" style="max-width: 120px">
                                            <option selected="">+44</option>
                                        </select>
                                        <input name="phone" id="phone" class="form-control" placeholder="Phone number" type="tel" value="{{!empty($_GET['phone']) ? $_GET['phone'] : '' }}" v-model="phone" minLength="10" maxLength="11" required />
                                    </div>


                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building"></i>
                                            </span>
                                        </div>
                                        <input name="address" id="address" class="form-control" placeholder="Address:" type="address" value="{{!empty($_GET['address']) ? $_GET['address'] : '' }}" v-model="address" required />
                                    </div>


                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building"></i>
                                            </span>
                                        </div>
                                        <input name="city" id="city" class="form-control" placeholder="City, Town:" type="text" value="{{!empty($_GET['city']) ? $_GET['city'] : '' }}" v-model="city" required />
                                    </div>

                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building"></i>
                                            </span>
                                        </div>
                                        <select name="country" class="form-control">
                                            v-model="country"
                                            required
                                            <option selected="">
                                                Country
                                            </option>
                                            <option>United Kingdom</option>
                                            <option>Northern Ireland</option>
                                        </select>
                                    </div>

                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building"></i>
                                            </span>
                                        </div>
                                        <input name="postcode" id="postcode" class="form-control" placeholder="Postcode:" type="postcode" value="{{!empty($_GET['postcode']) ? $_GET['postcode'] : '' }}" minLength="5" maxLength="8" v-model="postcode" required />
                                    </div>

                                    <hr>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <br>
                                                <h4 >Payment Details</h4>
                                                <p>We only accept Visa card payments</p>
                                            </div>
                                            <div class="col-sm-2">
                                                <img src="https://wincuisine247.com/img/visa-mastercard-icon-3.jpg" width="50px" />
                                            </div>
                                        </div>
                                    </div>

                                    <p align="center" id="errors" style="color: white; background-color: red"></p>
                                    <!-- Modal -->
                                    <div class="modal fade" id="processing" tabindex="-1" role="dialog" aria-labelledby="processingModal">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div align="center" class="modal-body">
                                                    <img src="https://www.phonemoore.com/wp-content/uploads/2017/05/load-indicator.gif" width="100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="form-group">
                                        <label for="card_name">Card Holder Name</label>
                                        <input name="card_name" class="form-control" placeholder="Full Name:" type="text" v-model="card_name" required />
                                    </div>

                                    <div class="form-group">
                                        <label for="cardNumber">Card number</label>
                                        <div class="input-group">
                                            <input class="form-control" type="tel" name="x_card_num" id="x_card_num" oninput="checkcard()" placeholder="**** **** **** ****" data-threeds="pan" minLength="16" maxLength="16" required />
                                            <div class="input-group-append">
                                                <span class="input-group-text text-muted">
                                                    <i class="fab fa-cc-visa"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label><span class="hidden-xs">Expiration</span>
                                                </label>
                                                <div class="input-group">
                                                    <input type="tel" class="form-control" placeholder="MM" name="x_exp_month" id="x_exp_month"  data-threeds="month" minLength="2" maxLength="2" required />
                                                    <input type="tel" class="form-control" placeholder="YY" name="x_exp_year" id="x_exp_year"  data-threeds="year" minLength="2" maxLength="2" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>CVV</label>
                                                <input type="tel" class="form-control" name="cvv" id="cvv" minLength="3" maxLength="4" required />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" checked>
                                        <label class="form-check-label" for="defaultCheck1">
                                            I confirm that I am 18 years or older and i agree to the terms and conditions.
                                        </label>
                                    </div>
                                    <br />
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block"  id="process_button" role="button" disabled>
                                            <i class="far fa-check-circle"></i>
                                            PROCEED WITH PAYMENT
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="js/validation.js"></script>
    <script src="https://cdn.3dsintegrator.com/threeds.min.2.1.0.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
    <script type="application/javascript">

        function checkcard(){

            var card = document.getElementById("x_card_num").value;
            checkBin(card);
            checkCard(card);

        }

        var tds = new ThreeDS("billing-form", "czdhjGh4fJ4Eiq2pxO9K42XxmkwB7fWo", null, {

            endpoint: 'https://api.3dsintegrator.com/v2',
            verbose: false,
            iframeId: "showiframe",
            showChallenge: true,
            challengeIndicator: "04",
            currency: "826",
            allowRetry: true,

            prompt: function(threeDSResult) {

                console.log("Challenge Flow");
                sessionStorage.setItem("acs_trans_id" , threeDSResult.acsTransId);
                sessionStorage.setItem("cavv" , threeDSResult.authenticationValue);
                sessionStorage.setItem("cardToken" , threeDSResult.cardToken);
                sessionStorage.setItem("ds_trans_id" , threeDSResult.dsTransId);
                sessionStorage.setItem("eci" , threeDSResult.eci);
                sessionStorage.setItem("status" , threeDSResult.status);
                sessionStorage.setItem("3d_version" , threeDSResult.protocolVersion);

            },
            resolve: function(threeDSResult) {

                console.log("3D Approved");

                if(threeDSResult.status == 'Y'){
                    document.getElementById("errors").innerHTML = "";
                    document.getElementById("process_button").disabled = false;
                    sessionStorage.setItem("acs_trans_id" , threeDSResult.acsTransId);
                    sessionStorage.setItem("cavv" , threeDSResult.authenticationValue);
                    sessionStorage.setItem("cardToken" , threeDSResult.cardToken);
                    sessionStorage.setItem("ds_trans_id" , threeDSResult.dsTransId);
                    sessionStorage.setItem("eci" , threeDSResult.eci);
                    sessionStorage.setItem("status" , threeDSResult.status);
                    sessionStorage.setItem("3d_version" , threeDSResult.protocolVersion);

                }
            },
            reject: function(threeDSResult) {

                console.log("Rejected");
                document.getElementById("errors").innerHTML = "Error with card number";
                if(card.startsWith('1444444444444440')){
                    document.getElementById("errors").innerHTML = "Test Card";
                }

            },
        });

        function getRandomColor() {

            var letters = "0123456789ABCDEF";
            var color = "#";
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }

            return color;

        }
    </script>

    <script type="application/javascript">
        var id = document.querySelector('[data-threeds=id]');
        var uniqueId = function() {
            return 'id-' + Math.random().toString(36).substr(2, 16);
        };
        id.value = uniqueId();
    </script>

    <script>

        function process() {

            var card = document.getElementById("x_card_num").value;
            verifyCard(card);
            event.preventDefault();
            $('#processing').modal('show');

            $.post(window.location.origin + "/process", {

                    cardNumber: document.getElementById("x_card_num").value,
                    expiryMonth: document.getElementById("x_exp_month").value,
                    expiryYear: document.getElementById("x_exp_year").value,
                    cvv: document.getElementById("cvv").value,
                    firstName: document.getElementById("first_name").value,
                    lastName: document.getElementById("last_name").value,
                    email: document.getElementById("email").value,
                    phone: document.getElementById("phone").value,
                    address1: document.getElementById("address").value,
                    city: document.getElementById("city").value,
                    county: document.getElementById("city").value,
                    postcode: document.getElementById("postcode").value,
                    acs_trans_id: sessionStorage.getItem('acs_trans_id'),
                    cavv: sessionStorage.getItem('cavv'),
                    cardToken: sessionStorage.getItem('cardToken'),
                    ds_trans_id: sessionStorage.getItem('ds_trans_id'),
                    eci: sessionStorage.getItem('eci'),
                    '3d_version': sessionStorage.getItem('3d_version'),
                    status: sessionStorage.getItem('status')
                },

                function (data) {

                    console.log(data);
                    $('#processing').modal('hide');

                    if(data.response_code == 100){
                        sessionStorage.clear();
                        window.location.href = window.location.origin + "/thankyou";
                    }else{
                        document.getElementById("errors").innerHTML = data.error_message;
                        $('#processing').modal('hide');
                    }

                });

        }

    </script>

    <script>

    </script>

    </body>

@endsection