<? use App\Campaign\FirePixel; ?>

<div align="center">
    <h2 style="background-color: seagreen; color: white; padding-top: 15px; padding-bottom: 15px">Congratulations! You can now win amazing prizes daily!</h2>
    <strong>Your login details are:</strong><br><br>
    Username : {{session('username')}} <br>
    Password : {{session('password')}}
    <p><strong>Please save these login details!</strong></p><br>
    <button type="submit" class="btn btn-success btn-lg"><h3>LUCKY NUMBER: {{session('step1-id')}}</h3></button>
    <br><br>
    <h2 style="background-color: seagreen; color: white; padding-top: 15px; padding-bottom: 15px"><strong>Link to Gaming Portal</strong></h2>
    <a href="//performscores.com/en/public/dashboard" target="_blank"><h3>CLICK HERE</h3></a>
</div>

<meta http-equiv="refresh" content="3; URL='{{ $redirects[0]->link_success }}'" />

{{FirePixel::pixelStep1()}}
{{--{{Session::flush()}}--}}