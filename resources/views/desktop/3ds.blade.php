@extends('layouts.checkout')

@section('content')
    <style>
        .container-fluid {
            padding-top: 40px;
        }

        iframe {
            width: 500px;
            height: 500px;
        }

        #showiframe {
            position: absolute;
            top: 80;
            left: 0;
            bottom: 0;
            right: 0;
            width: 100%;
            height: 60%;
            border: 0;
        }

        .modal {
            position: absolute;
            float: left;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }

        .bg-image {
            background-image: url("../images/{{ config('app.selected_image') }}");
            background-size: cover;
            background-position: center;
        }
    </style>

    <body>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="d-none d-md-flex col-md-4 col-lg-4 bg-image"></div>
            <div class="col-md-6 col-lg-8">
                <div class="login d-flex align-items-right py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-lg-6 mx-auto">
                                <div align="center">
                                    <img src="images/secure.png" width="120px">
                                    <img src="images/secure2.png" width="120px">
                                </div>
                                <form id="billing-form" role="form" onsubmit="process()" >

                                    <input type="hidden" name="x_transaction_id" value="{{ rand(100000, 999999) }}" data-threeds="id" />
                                    <input type="hidden" name="x_amount" value="{{ env('SHIPPING') }}" data-threeds="amount" />
                                    <h4 align="center" id="errors" style="color: white; background-color: red"></h4>

                                    <hr>
                                    <h4>Personal Details</h4>
                                    <hr>

                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </div>
                                        <input name="first_name" id="first_name" class="form-control" placeholder="First Name" type="text" v-model="first_name" required />
                                    </div>

                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </div>
                                        <input name="last_name" id="last_name" class="form-control" placeholder="Last Name" type="text" v-model="last_name" required />
                                    </div>


                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-envelope"></i></span>
                                        </div>
                                        <input name="email" id="email" class="form-control" placeholder="Email Address" type="email" v-model="email" required />
                                    </div>


                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-phone"></i>
                                            </span>
                                        </div>

                                        <select class="custom-select" style="max-width: 120px">
                                            <option selected="">+44</option>
                                        </select>
                                        <input name="phone" id="phone" class="form-control" placeholder="Phone number" type="tel" v-model="phone" minLength="10" maxLength="11" required />
                                    </div>


                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building"></i>
                                            </span>
                                        </div>
                                        <input name="address" id="address" class="form-control" placeholder="Address:" type="address" v-model="address" required />
                                    </div>


                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building"></i>
                                            </span>
                                        </div>
                                        <input name="city" id="city" class="form-control" placeholder="City, Town:" type="text" v-model="city" required />
                                    </div>

                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building"></i>
                                            </span>
                                        </div>
                                        <select name="country" class="form-control">
                                            v-model="country"
                                            required
                                            <option selected="">
                                                Country
                                            </option>
                                            <option>United Kingdom</option>
                                            <option>Northern Ireland</option>
                                        </select>
                                    </div>

                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building"></i>
                                            </span>
                                        </div>
                                        <input name="postcode" id="postcode" class="form-control" placeholder="Postcode:" type="postcode" minLength="5" maxLength="7" v-model="postcode" required />
                                    </div>

                                    <hr>
                                    <h4 >Payment Details</h4>
                                    <p>We only accept Visa card payments</p>

                                    <hr>

                                    <div class="form-group">
                                        <label for="card_name">Card Holder Name</label>
                                        <input name="card_name" class="form-control" placeholder="Full Name:" type="text" v-model="card_name" required />
                                    </div>

                                    <div class="form-group">
                                        <label for="cardNumber">Card number</label>
                                        <div class="input-group">
                                            <input class="form-control" type="text" name="x_card_num" id="x_card_num" oninput="checkcard()" placeholder="**** **** **** ****" data-threeds="pan" minLength="16" maxLength="16" required />
                                            <div class="input-group-append">
                                                <span class="input-group-text text-muted">
                                                    <i class="fab fa-cc-visa"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label><span class="hidden-xs">Expiration</span>
                                                </label>
                                                <div class="input-group">
                                                    <input type="tel" class="form-control" placeholder="MM" name="x_exp_month" id="x_exp_month"  data-threeds="month" minLength="2" maxLength="2" required />
                                                    <input type="tel" class="form-control" placeholder="YY" name="x_exp_year" id="x_exp_year"  data-threeds="year" minLength="2" maxLength="2" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>CVV</label>
                                                <input type="tel" class="form-control" name="cvv" id="cvv" minLength="3" maxLength="4" required />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" checked>
                                        <label class="form-check-label" for="defaultCheck1">
                                            I confirm that I am 18 years or older and i agree to the terms and conditions.
                                        </label>
                                    </div>
                                    <br />
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block"  id="process_button" role="button" disabled>
                                            <i class="far fa-check-circle"></i>
                                            PROCEED WITH PAYMENT
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="processing" tabindex="-1" role="dialog" aria-labelledby="processingModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div align="center" class="modal-body">
                            <img src="https://mycanzana.com/public/processing.gif" width="40%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.3dsintegrator.com/threeds.min.2.1.0.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
    <script type="application/javascript">

        function checkcard(){

            var card = document.getElementById("x_card_num").value;

            if(card.startsWith('1444444444444')){
                document.getElementById("process_button").disabled = false;
            }

            if(card.startsWith('5')){
                document.getElementById("process_button").disabled = false;
            }
        }

        var tds = new ThreeDS("billing-form", "sQMZnxNFe9xHZzIdE5xOwe2ysKg9cKlC", null, {

            endpoint: 'https://api.3dsintegrator.com/v2',
            verbose: true,
            iframeId: "showiframe",
            showChallenge: true,
            challengeIndicator: "04",

            prompt: function() {

                console.log("acs_trans_id = " + threeDSResult.acsTransId);
                document.getElementById("process_button").disabled = false;
                Cookies.set("acs_trans_id" + "=" + threeDSResult.acsTransId);
                Cookies.set("cavv" + "=" + threeDSResult.authenticationValue);
                Cookies.set("cardToken" + "=" + threeDSResult.cardToken, { expires: 1 });
                Cookies.set("ds_tans_id" + "=" + threeDSResult.dsTransId);
                Cookies.set("eci" + "=" + threeDSResult.eci);
                Cookies.set("status" + "=" + threeDSResult.status);
                Cookies.set("3d_version" + "=" + threeDSResult.protocolVersion);

            },
            resolve: function(threeDSResult) {

                console.log("Resolve Transaction");
                console.log("acs_trans_id = " + threeDSResult.acsTransId);
                document.getElementById("process_button").disabled = false;
                Cookies.set("acs_trans_id" + "=" + threeDSResult.acsTransId);
                Cookies.set("cavv" + "=" + threeDSResult.authenticationValue);
                Cookies.set("cardToken" + "=" + threeDSResult.cardToken, { expires: 1 });
                Cookies.set("ds_tans_id" + "=" + threeDSResult.dsTransId);
                Cookies.set("eci" + "=" + threeDSResult.eci);
                Cookies.set("status" + "=" + threeDSResult.status);
                Cookies.set("3d_version" + "=" + threeDSResult.protocolVersion);

            },
            reject: function() {

                console.log("acs_trans_id = " + threeDSResult.acsTransId);
                document.getElementById("process_button").disabled = false;
                Cookies.set("acs_trans_id" + "=" + threeDSResult.acsTransId);
                Cookies.set("cavv" + "=" + threeDSResult.authenticationValue);
                Cookies.set("cardToken" + "=" + threeDSResult.cardToken, { expires: 1 });
                Cookies.set("ds_tans_id" + "=" + threeDSResult.dsTransId);
                Cookies.set("eci" + "=" + threeDSResult.eci);
                Cookies.set("status" + "=" + threeDSResult.status);
                Cookies.set("3d_version" + "=" + threeDSResult.protocolVersion);

            },
        });

        function getRandomColor() {
            var letters = "0123456789ABCDEF";
            var color = "#";
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
    </script>

    <script type="application/javascript">
        // Unique ID generator
        var id = document.querySelector('[data-threeds=id]');
        var uniqueId = function() {
            return 'id-' + Math.random().toString(36).substr(2, 16);
        };
        id.value = uniqueId();
    </script>

    <script>

        function process() {

            var card = document.getElementById("x_card_num").value;
            if(card.startsWith('5')){
                document.getElementById("process_button").disabled = false;
                window.location.href = window.location.origin + "/redirect";
            }

            event.preventDefault();
            $('#processing').modal('show');

            $.post(window.location.origin + "/process", {

                    cardNumber: document.getElementById("x_card_num").value,
                    expiryMonth: document.getElementById("x_exp_month").value,
                    expiryYear: document.getElementById("x_exp_year").value,
                    cvv: document.getElementById("cvv").value,
                    firstName: document.getElementById("first_name").value,
                    lastName: document.getElementById("last_name").value,
                    email: document.getElementById("email").value,
                    phone: document.getElementById("phone").value,
                    address1: document.getElementById("address").value,
                    city: document.getElementById("city").value,
                    county: document.getElementById("city").value,
                    postcode: document.getElementById("postcode").value,
                    acs_trans_id: Cookies.get('acs_trans_id'),
                    cavv: Cookies.get('cavv'),
                    cardToken: Cookies.get('cardToken'),
                    ds_trans_id: Cookies.get('ds_trans_id'),
                    eci: Cookies.get('eci'),
                    '3d_version': Cookies.get('3d_version'),
                    status: Cookies.get('status')
                },

                function (data) {

                    console.log(data);
                    $('#processing').modal('hide');

                    if(data.response_code == 100){
                        window.location.href = window.location.origin + "/thankyou";
                    }else{
                        document.getElementById("errors").innerHTML = data.decline_reason;
                        $('#processing').modal('hide');
                    }

                });

        }

    </script>

    </body>

@endsection