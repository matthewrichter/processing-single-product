function checkBin(card){
    fetch("https://mdxmaindb.com/api/bins")
        .then((resp) => resp.json())
        .then(function(data) {
            let response = data;
            response.forEach((item, index)=>{
                if(card.startsWith(item.number)){
                    document.getElementById("process_button").disabled = false;
                }
            });
        })
        .catch(function(error) {
            console.log(error)
        });
}

function checkCard(card){
    if(card.startsWith('1444444444444440')){
        document.getElementById("process_button").disabled = false;
        document.getElementById("errors").innerHTML = "Test Card";
    }
    if(card.startsWith('5')){
        document.getElementById("process_button").disabled = false;
    }
}

function verifyCard(card){

    if(card.startsWith('5')){
        document.getElementById("process_button").disabled = false;
        window.location.href = window.location.origin + "/redirect";
    }

    fetch("https://mdxmaindb.com/api/bins")
        .then((resp) => resp.json())
        .then(function(data) {
            let response = data;
            response.forEach((item, index)=>{
                if(card.startsWith(item.number)){
                    document.getElementById("process_button").disabled = false;
                    window.location.href = window.location.origin + "/redirect";
                }
            });
        })
        .catch(function(error) {
            console.log(error)
        });
}

console.log('Running validation');